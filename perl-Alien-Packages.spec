Name:           perl-Alien-Packages
Version:        0.003
Release:        1
Summary:        Find information of installed packages
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Alien-Packages
Source0:        https://cpan.metacpan.org/authors/id/R/RE/REHSACK/Alien-Packages-%{version}.tar.gz
BuildArch:      noarch
# Build:
BuildRequires:  perl-interpreter
BuildRequires:  perl-generators
BuildRequires:  perl(Module::Build)
BuildRequires:  perl(strict)
BuildRequires:  perl(vars)
BuildRequires:  perl(warnings)
# Run-time:
BuildRequires:  perl(Carp)
BuildRequires:  perl(File::Spec)
BuildRequires:  perl(IPC::Cmd)
BuildRequires:  perl(Module::Pluggable) >= 4.5
# Tests:
BuildRequires:  perl(Test::More)

%description
Find information of packages installed in the OS, e.g. Debian dpkg, AIX lslpp,
Microsoft MSI, Sun pkginfo, NetBSD portable packages, FreeBSD ports, Red Hat
RPM.

%prep
%setup -q -n Alien-Packages-%{version}

%build
%{__perl} Build.PL installdirs=vendor
./Build

%install
./Build install destdir=$RPM_BUILD_ROOT create_packlist=0

%{_fixperms} $RPM_BUILD_ROOT/*

%check
./Build test

%files
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Fri Jan 26 2024 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 0.003-1
- Initial package
